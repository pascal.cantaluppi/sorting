package ch.ffhs;

import ch.ffhs.sort.Insertion;
import ch.ffhs.sort.Selection;
import ch.ffhs.sort.Shell;

/**
 * sorting base class
 */
public class Sorting {

    public enum SortType {
        SELECTION,
        INSERTION,
        SHELL
    }

    public static String[] sort(String[] a, SortType type) {
        switch(type) {
            case SELECTION:
                a = Selection.sort(a);
                break;
            case INSERTION:
                a = Insertion.sort(a);
                break;
            case SHELL:
                a = Shell.sort(a);
                break;
        }
        return a;
    }

    public static boolean less(String x, String y) {
        return x.compareTo(y) < 0;
    }

    /*
    public static void swap(String[] a, int i, int j) {
        String temp = a[i];
        a[i] = a[j];
        a[j] = temp;
    }
    */

    public static void show(String[] a){
        for(int i = 0; i < a.length; i++) {
            System.out.print(a[i] + " ");
        }
        System.out.println();
    }

    public static boolean isSorted(String[] a) {
        for(int i = 1; i < a.length; i++) {
            if(less(a[i], a[i-1])) {
                return false;
            }
        }
        return true;
    }

}

