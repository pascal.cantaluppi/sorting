package ch.ffhs;

import java.util.Arrays;

/**
 * data structures and algorithms - sorting
 *
 * @author pascal cantaluppi
 * @version 1.0
 */
public class Main {
    /**
     * the main class launches the application.
     * @param args command line parameters
     */
    public static void main(String[] args) {
        String array[] = {"one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten"};

        System.out.println(Arrays.toString(array));
        System.out.println(Sorting.isSorted(array) ? "sorted" : "unsorted");
        System.out.println();

        String newArray[] = Sorting.sort(array, Sorting.SortType.SELECTION);
        System.out.println(Sorting.isSorted(newArray) ? "sorted" : "unsorted");
        Sorting.show(newArray);
        System.out.println();

        long startTime = System.nanoTime();
        Sorting.sort(array, Sorting.SortType.SELECTION);
        long endTime   = System.nanoTime();
        long totalTime = endTime - startTime;
        System.out.println("Runtime selection: " + totalTime);
        startTime = System.nanoTime();
        Sorting.sort(array, Sorting.SortType.INSERTION);
        endTime   = System.nanoTime();
        totalTime = endTime - startTime;
        System.out.println("Runtime insertion: " + totalTime);
    }
}
