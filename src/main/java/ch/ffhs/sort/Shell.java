package ch.ffhs.sort;

import ch.ffhs.Sorting;

/**
 * shellsort class
 */
public class Shell {

    public static String[] sort(String[] array) {
        for (int gapSize = array.length / 2; gapSize > 0; gapSize /= 2)
        {
            for (int currentIndex = gapSize; currentIndex < array.length; currentIndex++)
            {
                int currentIndexCopy = currentIndex;
                String item = array[currentIndex];
                while (currentIndexCopy >= gapSize && array[currentIndexCopy - gapSize].compareTo(item) > 0)
                {
                    array[currentIndexCopy] = array[currentIndexCopy - gapSize];
                    currentIndexCopy -= gapSize;
                }
                array[currentIndexCopy] = item;
            }
        }
        return array;
    }
}
