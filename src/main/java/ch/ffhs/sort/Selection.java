package ch.ffhs.sort;

import ch.ffhs.Sorting;

/**
 * selectionsort class
 */
public class Selection {

    public static String[] sort(String[] array) {
        for (int currentIndex = 0; currentIndex < array.length - 1; currentIndex++)
        {
            int minIndex = currentIndex;
            for (int i = currentIndex + 1; i < array.length; i++)
            {
                if (array[i].compareTo(array[minIndex]) < 0)
                {
                    minIndex = i;
                }
            }
            if (minIndex != currentIndex)
            {
                String temp = array[currentIndex];
                array[currentIndex] = array[minIndex];
                array[minIndex] = temp;
            }
        }
        return array;
    }
}
