package ch.ffhs.sort;

import ch.ffhs.Sorting;

/**
 * insertionsort class
 */
public class Insertion {

    public static String[] sort(String[] array) {
        for (int i = 1; i < array.length; i++)
        {
            int currentIndex = i;
            while (currentIndex > 0 && array[currentIndex - 1].compareTo(array[currentIndex]) > 0)
            {
                String temp = array[currentIndex];
                array[currentIndex] = array[currentIndex - 1];
                array[currentIndex - 1] = temp;
                currentIndex--;
            }
        }
        return array;
    }
}
