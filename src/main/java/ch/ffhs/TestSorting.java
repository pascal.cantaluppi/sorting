package ch.ffhs;

import org.testng.annotations.Test;

import static org.junit.jupiter.api.Assertions.*;

class TestSorting {

    String array[] = {"one", "two", "three", "four"};

    @org.junit.jupiter.api.BeforeEach
    void setUp() {
    }

    @org.junit.jupiter.api.AfterEach
    void tearDown() {
        array = null;
    }

    @org.junit.jupiter.api.Test
    void insertion() {
        String newArray[] = Sorting.sort(array, Sorting.SortType.INSERTION);
        assertEquals("four", newArray[0], "First element should be 'four'");
    }

    @org.junit.jupiter.api.Test
    void selectiopn() {
        String newArray[] = Sorting.sort(array, Sorting.SortType.SELECTION);
        assertEquals("four", newArray[0], "First element should be 'four'");
    }

    @org.junit.jupiter.api.Test
    void shell() {
        String newArray[] = Sorting.sort(array, Sorting.SortType.SHELL);
        assertEquals("four", newArray[0], "First element should be 'four'");
    }

}